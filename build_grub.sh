#!/bin/bash
set -euo pipefail

# Set variables
grub_commit="9c34d56c2dafcd2737db0e3e49df63bce4d8b504"

modules_bios="biosdisk fat normal part_gpt part_msdos"
SB_FS_MODULES="btrfs exfat ext2 fat hfsplus iso9660 jfs ntfs part_apple part_bsd part_msdos part_gpt reiserfs udf ufs1 ufs1_be ufs2 xfs zfs zfscrypt zfsinfo zstd"
SB_NET_MODULES="efinet http tftp"
SB_GRUB_MODULES="acpi all_video at_keyboard boot bsd cat chain cmp configfile cpuid datehook dm_nv echo efifwsetup eval font gettext gfxmenu gfxterm gfxterm_background \
	gzio halt help hexdump jpeg keylayouts keystatus loadenv loopback linux ls lsacpi lsefi lsefimmap lsefisystab lssal lvm lzopio mdraid09 mdraid09_be mdraid1x \
	memdisk minicmd multiboot multiboot2 newc normal png probe raid5rec raid6rec read reboot regexp search search_fs_uuid search_fs_file search_label serial sleep \
	smbios squash4 test tr true video videoinfo usb_keyboard xzio"
SB_CRYPTO_MODULES="cryptodisk gcry_arcfour gcry_blowfish gcry_camellia gcry_cast5 gcry_crc gcry_des gcry_dsa gcry_idea gcry_md4 gcry_md5 \
	gcry_rfc2268 gcry_rijndael gcry_rmd160 gcry_rsa gcry_seed gcry_serpent gcry_sha1 gcry_sha256 gcry_sha512 gcry_tiger gcry_twofish \
	gcry_whirlpool geli luks luks2 password_pbkdf2 tpm"
SB_MODULES="${SB_FS_MODULES} ${SB_NET_MODULES} ${SB_GRUB_MODULES} ${SB_CRYPTO_MODULES}"

# sbat data
distro_id="multiosusb"
distro_name="MultiOS-USB"
upstream_sbat="4"
distro_sbat="2"
pkg_name="grub"
version="2.12"

home_dir=$(pwd)
mkdir ${home_dir}/logs

# Download grub2 source
git clone --single-branch https://git.savannah.gnu.org/git/grub.git && cd grub
git checkout ${grub_commit}

git apply ../0001-Change-default-sectors-GPT.patch
git apply ../0002-grub2-secureboot-chainloader.patch
git apply ../0003-grub2-efi-chainload-harder.patch
git apply ../0004-Added-aliases-linuxefi-and-initrdefi.patch

git add .
git config user.email "gitlab" && git config user.name "gitlab"
git commit -m "Added patches for MultiOS-USB"
git tag grub-src

# Fix DejaVuSans.ttf location so that grub-mkfont can create *.pf2 files for starfield theme...
if [[ -f "/etc/os-release" ]]; then
	distroname=$(grep -w ID /etc/os-release | sed 's/ID=//g' | tr -d '="')

	if [[ "$distroname" == "arch" || "$distroname" == "manjaro" ]]; then
		sed -i 's|/usr/share/fonts/dejavu|/usr/share/fonts/dejavu /usr/share/fonts/TTF|g' configure.ac
	elif [[ "$distroname" == "debian" || "$distroname" == "ubuntu" ]]; then
		sed -i 's|/usr/share/fonts/X11/misc|/usr/share/fonts/X11/misc /usr/share/fonts/truetype/dejavu|g' configure.ac
	fi
fi

# Bootstrap sources
./bootstrap

# Begin build for x86_64-efi
mkdir ${home_dir}/build_x86_64-efi && cd ${home_dir}/build_x86_64-efi
unset CFLAGS
unset CPPFLAGS
unset CXXFLAGS
unset LDFLAGS
unset MAKEFLAGS

../grub/configure \
	--prefix=/ \
	--target=x86_64 \
	--with-platform=efi \
	--disable-efiemu

make -j$(nproc) &> ../logs/make_x86_64-efi.log

cat > sbat.csv << EOF
sbat,1,SBAT Version,sbat,1,https://github.com/rhboot/shim/blob/main/SBAT.md
grub,${upstream_sbat},Free Software Foundation,grub,${version},https://www.gnu.org/software/grub/
grub.${distro_id},${distro_sbat},${distro_name},${pkg_name},${version},https://gitlab.com/MultiOS-USB/grub
EOF

mkdir -p memdisk/fonts	
cp unicode.pf2 memdisk/fonts	
mksquashfs memdisk memdisk.squashfs -comp xz

./grub-mkimage \
	--format="x86_64-efi" \
	--output="../grubx64.efi" \
	--directory="grub-core" \
	--memdisk="memdisk.squashfs" \
	--prefix="/grub" \
	--sbat="sbat.csv" \
	--compression="auto" \
	--verbose \
	$SB_MODULES &> ../logs/mkimage_x86_64-efi.log

./grub-editenv ../grubenv create
# End build for x86_64-efi

# Begin build for i386-pc
mkdir ${home_dir}/build_i386-pc && cd ${home_dir}/build_i386-pc
unset CFLAGS
unset CPPFLAGS
unset CXXFLAGS
unset LDFLAGS
unset MAKEFLAGS

../grub/configure \
	--prefix=/ \
	--target=i386 \
	--with-platform=pc \
	--disable-efiemu

make -j$(nproc) &> ../logs/make_i386-pc.log
(cd grub-core && make install DESTDIR=${home_dir}/PKG_i386-pc)
cp unicode.pf2 ../

./grub-mkimage \
	--format="i386-pc" \
	--prefix="(,gpt1)/grub" \
	--directory="grub-core" \
	--output="../PKG_i386-pc/lib/grub/i386-pc/core.img" \
	--compression="auto" \
	--verbose \
	$modules_bios &> ../logs/mkimage_i386-pc.log
# End build for i386-pc

# Go to home folder
cd ${home_dir}

# Write ReadMe.txt
cat > ReadMe.txt << EOF
Grub base source code:
https://git.savannah.gnu.org/cgit/grub.git/tree/?id=${grub_commit}
EOF
    
# Clean unneeded files
rm -f PKG_*/lib/grub/*/*.module
rm -f PKG_*/lib/grub/*/*.image
rm -f PKG_*/lib/grub/*/kernel.exec
rm -f PKG_*/lib/grub/*/{gdb_grub,gdb_helper.py}

# Checking the size of the core.img file
coreimg_size=$(stat -c %s PKG_i386-pc/lib/grub/i386-pc/core.img)
coreimg_size_max=$((512*(2048-34)))	# 1031168
if [[ $coreimg_size -gt $coreimg_size_max ]]; then 
	echo "File core.img is too large (> $coreimg_size_max bytes)!"
	exit 1
fi

tar -cJf i386-pc.tar.xz -C PKG_i386-pc/lib/grub/ i386-pc
